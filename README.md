# Announcement Telegram Bot

Prototype of a Telegram Bot which handles the announcement messages of Climatestrike Switzerland.

## Running the bot

Install all the dependencies using npm:
```shell
$ npm i
```

Create a .env file with the required variables (see [Environment Variables](#environment-variables))

Start the bot with npm:
```shell
$ npm run dev
```

This will create a developement build to create a production ready build use:
```shell
$ npm run build
$ npm run start
```

## Contributing

### Linting

Please always lint your code before comitting your changes:
```shell
$ npm run lint      # This will just lint your files
$ npm run lint:fix  # This will automatically try to fix all problems
```

### Commit messages

Write your commit messages carefully. Try to stick to the style described in [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/).

## Environment Variables

For reference see `example.env`. To use it insert content and rename/copy to `.env`.

**Do not commit changes to the `.env` file!**