import * as dotenv from 'dotenv'
dotenv.config()

import { Telegraf } from 'telegraf'

const bot = new Telegraf(process.env.TELEGRAM_TOKEN)    // Initialize bot with our token from .env

bot.start(ctx => ctx.reply('Hello World! 🔥'))          // Register a lsitener for the /start event
bot.on('text', (ctx) => ctx.reply('text'))              // Regester a listener for recieving text

bot.launch()    // Start the bot
console.log('Bot started ✔')